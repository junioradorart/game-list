import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public cachedGames:any[];

  constructor () {
    this.cachedGames = [];
  }

  setCachedGames(games:any) {
    games.forEach(el => {
      this.cachedGames[el.game._id] = el;
    });
  }

  getCachedGame(id:number) {
    return this.cachedGames[id];
  }
}
