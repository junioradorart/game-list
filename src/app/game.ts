export class Game {
  id: number;
  name: string;
  image: string;
  popularity: number;
  vizualizations: number;
  channels: number;
}
