import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-game-detail',
  templateUrl: './game-detail.component.html',
  styleUrls: ['./game-detail.component.scss']
})
export class GameDetailComponent implements OnInit {
  public currentGame: any;
  public title: any;

  constructor(private router: Router, private route: ActivatedRoute, private appComponent: AppComponent) {
    this.title = '';
    this.route.params.subscribe(params => this.resolveParams(params));
  }

  ngOnInit() {
  }

  resolveParams(params:any) {
    if (params.id) {
      this.currentGame = this.appComponent.getCachedGame(params.id);
    }
  }
}
