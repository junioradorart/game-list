import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Http } from '@angular/http';
import { Game } from './game';

@Injectable()
export class GameService {
  private gamesUrl: string;
  public games: any;
  private clientId: string;
  private subscribeCallback:any;

  constructor( private _http: HttpClient ) {
    this.gamesUrl = 'https://api.twitch.tv/kraken/games/top?limit=12';
    this.clientId = 'il6gmpiwf45t5xnka8244b3hszjdtf';
  }

  loadGames() {
    let headers = {'Client-ID': this.clientId};

    this._http.get(this.gamesUrl, {headers: headers})
        .subscribe(games => {
          this.games = games;
          if (this.subscribeCallback) {
            this.subscribeCallback();
          }
        });
  }

  setSubscribeCallback(cbk:any) {
    this.subscribeCallback = cbk;
  }

  getGames() {
    return this.games;
  }
}
