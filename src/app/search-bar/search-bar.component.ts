import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { GameListComponent } from '../game-list/game-list.component';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit {
  public games: any;
  private filter: string;
  private search: string;

  constructor(public gameList: GameListComponent) {
    this.filter = undefined;
    this.search = "";
  }

  ngOnInit() {

  }

  setFilter(value: string) {
    if (!value) {
      return;
    }

    this.gameList.setFilter(value);

    this.filter = value;
  }

  setSearch() {
    this.gameList.setSearch(this.search);
  }
}
