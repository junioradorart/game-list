import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { GameService } from './game.service';
import { SearchPipe } from './search-filter.pipe';
import { ReplacePipe } from './replace-str.pipe';
import { FormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { ListItemsComponent } from './list-items/list-items.component';
import { AppRoutingModule } from './/app-routing.module';
import { GameListComponent } from './game-list/game-list.component';
import { GameDetailComponent } from './game-detail/game-detail.component';
import { TitleBarComponent } from './title-bar/title-bar.component';


@NgModule({
  declarations: [
    AppComponent,
    SearchBarComponent,
    ListItemsComponent,
    SearchPipe,
    ReplacePipe,
    GameListComponent,
    GameDetailComponent,
    TitleBarComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [GameService, GameDetailComponent, GameListComponent, TitleBarComponent, AppComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
