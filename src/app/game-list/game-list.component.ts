import { Component, OnInit } from '@angular/core';
import { GameService } from '../game.service';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-game-list',
  templateUrl: './game-list.component.html',
  styleUrls: ['./game-list.component.scss']
})
export class GameListComponent implements OnInit {

  public title:string = 'Game List';
  public requestGames:any;
  public search:string;
  public filter:string;
  public games:any;

  constructor (private gameService: GameService, private appComponent: AppComponent) {
    this.search = "";
    this.filter = "";
  }

  ngOnInit() {
    this.getGames();
    this.requestGames = this.gameService;
    this.gameService.setSubscribeCallback(() => {
      this.setGames(this.gameService.games);
      this.appComponent.setCachedGames(this.gameService.games.top);
    });
  }

  getGames() {
    this.gameService.loadGames();
  }

  orderByPopularity() {
    let arrPopularity = [];
    let arrSorted = [];

    this.games.top.forEach((el) => {
      arrPopularity[el.game.popularity] = el;
    });

    let arrSort = Object.keys(arrPopularity);

    arrSort.forEach((el) => {
      arrSorted.push(arrPopularity[el]);
    });

    this.games.top = arrSorted;
  }

  orderByVisualization() {
    let arrVisualization = [];
    let arrSorted = [];

    this.games.top.forEach((el) => {
      arrVisualization[el.viewers] = el;
    });

    let arrSort = Object.keys(arrVisualization);

    arrSort.forEach((el) => {
      arrSorted.push(arrVisualization[el]);
    });

    this.games.top = arrSorted;
  }

  setGames (value:any) {
    this.games = value;
  }

  setFilter(value:string) {
    if (value === 'popularity') {
      this.orderByPopularity();

    } else if (value === 'visualization') {
      this.orderByVisualization();
    }
  }

  setSearch(value:string) {
    this.search = value;
  }

}
