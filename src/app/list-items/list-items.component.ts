import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { GameDetailComponent } from '../game-detail/game-detail.component';

@Component({
  selector: 'app-list-items',
  templateUrl: './list-items.component.html',
  styleUrls: ['./list-items.component.scss']
})
export class ListItemsComponent implements OnInit {
  @Input("requestGames") requestGames: any;
  @Input("search") search: any;
  @Input("filter") filter: any;
  @Input("games") games: any;

  constructor(private router: Router, private gameDetail: GameDetailComponent) {

  }

  setGames(value: any) {
    this.games = value;
  }

  ngOnInit() {

  }

  onSelect(game: any) {
    var that = this;
    this.router.navigateByUrl('/game/' + game.game._id, { skipLocationChange: true });
  }

}
