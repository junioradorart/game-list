import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'searchFilter'
})
export class SearchPipe implements PipeTransform {
  public transform(value, searchValue: string, term: string) {
    if (searchValue) {
      var reg = new RegExp('^' + searchValue, 'i');
      return value.filter((el) => el.game.name.match(reg));

    } else {
      return value;
    }
  }
}
